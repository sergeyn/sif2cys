#include <fstream>

template<typename M, typename K>
inline unsigned checkAndSet(M& m, const K& key) {
    if(m.count(key) == 0) {
        const unsigned id =  m.size();
        return (m[key] = id);
    }
    return m[key];
}

template<typename T>
inline void checkAndAdd(unsigned id, const T& v, std::vector<T>& vec) {
	if(vec.size()<=id) 
		vec.push_back(v);
}

struct Sif {
    std::map<std::string,unsigned> label2nodeId;
    std::vector<std::string> node2Label;
    std::map<std::string,unsigned> label2edgeId;
    std::vector<std::string> edge2Label;
    std::vector<std::pair < unsigned, unsigned > > edges;
//--------------------------------------------------------
    virtual void print(std::ostream& out) {}
//--------------------------------------------------------
    virtual ~Sif() {}
//--------------------------------------------------------
    Sif(){}
    explicit Sif(const std::string& sifFName) {
        std::ifstream f(sifFName.c_str());
        std::string ns,e,nt;
        double len = 50;
        //node-s edge node-t double-length
        while(f.good()) {
            f >> ns >> e >> nt;
            unsigned nsId = checkAndSet(label2nodeId,ns);
            checkAndAdd(nsId,ns,node2Label);
            unsigned ntId = checkAndSet(label2nodeId,nt);
            checkAndAdd(ntId,nt,node2Label);
            checkAndSet(label2edgeId,e);
            edges.push_back(std::make_pair(nsId,ntId));
            edge2Label.push_back(e);
        }
    }
};
