#include <boost/config.hpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include <sstream>
#include "sif.h"


size_t writeEdges(std::ostream& out, const Sif& sif, const std::vector<unsigned>& labels, unsigned component) {
	size_t count = 0;
	for(unsigned i=0; i<sif.edges.size(); ++i) {
		std::pair<unsigned,unsigned> e = sif.edges[i];
		if(labels[e.first] == component) {
			++count;
			out << sif.node2Label[e.first] << " " << sif.edge2Label[i] << " " << sif.node2Label[e.second] << "\n";
		}
	}
	return count;
}

std::vector<size_t> histogram(const std::vector<unsigned>& labels, unsigned count) {
	std::vector<size_t> hist(count,0);
	for(unsigned i=0; i<labels.size(); ++i)
		++hist[labels[i]];
	return hist;
}

std::vector<std::pair<size_t,unsigned> > sortComponents(const std::vector<size_t>& hist) {
	std::vector<std::pair<size_t,unsigned> > res;
	for(unsigned i=0; i<hist.size(); ++i) 
			res.push_back(std::make_pair(hist[i],i));
	std::sort(res.begin(),res.end());
	return res;
}

int main(int argc, char* argv[]) 
{
  using namespace boost;
  {
    typedef adjacency_list <vecS, vecS, undirectedS> Graph;

	std::string siff(argv[1]);
	Sif sif(siff); //load sif
	
	std::cout << "Got " << sif.edges.size() << " edges between " << sif.node2Label.size() << " nodes\n";
	
	std::vector<unsigned> component(sif.label2nodeId.size());	
	unsigned numComps = 0;
	{
		Graph G;
		for(size_t i=0; i<sif.edges.size(); ++i)
			add_edge(sif.edges[i].first,sif.edges[i].second, G);
    
		numComps = connected_components(G, &component[0]);
	}
    
    std::cout << "Mapped nodes to components\n";
    std::vector<std::pair<size_t,unsigned> > hist = sortComponents(histogram(component,numComps));
    for(unsigned i=0; i<hist.size(); ++i) {
		std::cout << "Component " << hist[i].second << " has " << hist[i].first << " nodes \n";
	}
    
    unsigned threshold = argc>2?atoi(argv[2]):0; //set file threshold

    std::ostringstream fname;
    std::ofstream out;
    fname << siff << "_" << 0 << ".sif";
	std::cout << "writing to " << fname.str() << "\n";
	out.open(fname.str().c_str());
			
    size_t acc = 0;
	for(unsigned c = 0; c<hist.size(); ++c) { //for each component
		acc += writeEdges(out, sif, component, hist[c].second);
		if(acc>threshold) {
			acc = 0;
			fname.str("");
			fname << siff << "_" << c+1 << ".sif";
			std::cout << "writing to " << fname.str() << "\n";
			out.close();
			out.open(fname.str().c_str());
		}

	}
  }
  return 0;
}
