# README #

Ruby and C++ versions of a network (in .sif) to Cytoscape format conversions

Supports 
sif -> desktop Cytoscape session creation
sif -> Cytoscape JS elements (with cola layout)
weighted edges sif -> Cytoscape JS elements (with cola layout) + scaling
Note: the server backend and client frontend side parts of CyToServe(r) are currently in standalone repos above this one.
A single repo. with all the code is in the making. 