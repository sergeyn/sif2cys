constrained.o: constrained.cpp
	g++ -DHAVE_CONFIG_H -I. -I./libcola/  -Wall -W -Wpointer-arith -Wcast-align -Wsign-compare -Woverloaded-virtual -Wswitch  -g -O2 -MT constrained.o -MD -MP -MF constrained.Tpo -c -o constrained.o constrained.cpp

layout: constrained.o
	g++ constrained.o -L ./ -l cola -l vpsc -o layout

all: layout 

clean:
	rm -rf layout constrained.o
