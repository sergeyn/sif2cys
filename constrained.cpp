/*
 * vim: ts=4 sw=4 et tw=0 wm=0
 *
 * libcola - A library providing force-directed network layout using the 
 *           stress-majorization method subject to separation constraints.
 *
 * Copyright (C) 2006-2008  Monash University
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library in the file LICENSE; if not, 
 * write to the Free Software Foundation, Inc., 59 Temple Place, 
 * Suite 330, Boston, MA  02111-1307  USA
 *
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <float.h>
#include <map>

#include <cola.h>

inline double getRand(double range) {
    return range*rand()/RAND_MAX;
}

using namespace cola;

template<typename M, typename K>
inline unsigned checkAndSet(M& m, const K& key) {
    if(m.count(key) == 0) {
        const unsigned id =  m.size();
        return (m[key] = id);
    }
    return m[key];
}


void printCyJsPrefix(std::ostream& out) {
	out << "{\n\
  \"format_version\" : \"1.0\",\n\
  \"generated_by\" : \"cytoscape-3.1.1\",\n\
  \"target_cytoscapejs_version\" : \"~2.1\",\n\
  \"data\" : {\n\
    \"selected\" : true,\n\
    \"__Annotations\" : [ \"\" ],\n\
    \"shared_name\" : \"network.sif\",\n\
    \"SUID\" : 52,\n\
    \"name\" : \"network.sif\"\n\
  },\n\
  \"elements\" :\n";

}

struct Sif {
    std::map<std::string,unsigned> label2nodeId;
    std::vector<std::string> node2Label;
    std::map<std::string,unsigned> label2edgeId;
	vpsc::Rectangles nodes;
    std::vector<std::pair < unsigned, unsigned > > edges;
    EdgeLengths idealLengths;
//--------------------------------------------------------
    void print(std::ostream& out) {
		out << "{ \"nodes\": [\n";
		const unsigned size = label2nodeId.size();
		node2Label.resize(size);
		unsigned i=0;
		std::map<std::string,unsigned>::const_iterator it;
        for(it = label2nodeId.begin(); i<size-1; ++it, ++i)        { 
            out << "{ \"data\": { \"id\": \"" << it->first << "\" }, \"position\": {\"x\": " << nodes[it->second]->getCentreX() << ", \"y\": "<< nodes[it->second]->getCentreY() << "} },\n" ;  
			node2Label[it->second] = it->first;
        }
        out << "{ \"data\": { \"id\": \"" << it->first << "\" }, \"position\": {\"x\": " << nodes[it->second]->getCentreX() << ", \"y\": "<< nodes[it->second]->getCentreY() << "} }\n" ;  
        node2Label[it->second] = it->first;
        	
        out << "],\n\"edges\": [\n";
        
        std::map<std::string,unsigned>::const_iterator lit;
        const unsigned lsize = label2edgeId.size();
        for(i=0, it = label2edgeId.begin(); i<lsize-1; ++it, ++i) {
			out << "{ \"data\": { \"id\": \"" << it->first << "\", \"source\": \"" <<  node2Label[edges[it->second].first] << "\", \"target\": \"" << node2Label[edges[it->second].second] << "\" } },\n";
		}
        out << "{ \"data\": { \"id\": \"" << it->first << "\", \"source\": \"" <<  node2Label[edges[it->second].first] << "\", \"target\": \"" << node2Label[edges[it->second].second] << "\" } }";
        out << "]\n}" << std::endl;
    }
//--------------------------------------------------------
	void scale(const double baseLen=50.0) {
		const double minD = *std::min_element(idealLengths.begin(),idealLengths.end());
		for(unsigned i=0; i<idealLengths.size(); ++i)
			idealLengths[i]*=(baseLen/minD);
	}
//--------------------------------------------------------
    ~Sif() {
	    for(unsigned i=0;i<nodes.size(); ++i) {
		    delete nodes[i];
    	}
    }
//--------------------------------------------------------
    Sif(){}
    explicit Sif(const std::string& sifFName, bool hasLen) {
        std::ifstream f(sifFName.c_str());
        std::string ns,e,nt;
        double len = 50;
        //node-s edge node-t double-length
        while(f.good()) {
            f >> ns >> e >> nt;
            if(hasLen)
				f >> len;
            unsigned nsId = checkAndSet(label2nodeId,ns);
            unsigned ntId = checkAndSet(label2nodeId,nt);
            checkAndSet(label2edgeId,e);
            idealLengths.push_back(len);
            edges.push_back(std::make_pair(nsId,ntId));
        }
	    
        const unsigned V = label2nodeId.size();
        nodes.resize(V);
	    for(unsigned i=0;i<V;i++) 
		    nodes[i] = new vpsc::Rectangle(0,5,0,5);
    }
};
//--------------------------------------------------------
int main(int argc, char * argv[]) {

	std::string fname;
	if(argc < 5) {
		std::cerr << "usage:\nlayout network.sif width height hasLength(0 or 1)\n";
		return -1;
	}
	
	fname = argv[1];	
	double width=atoi(argv[2]);
	double height=atoi(argv[3]);
	bool hasLen = atoi(argv[4]);

    Sif sif(fname,hasLen);
    if(sif.nodes.size() * sif.edges.size() == 0) {
        std::cerr << "couldn't read the network from " << fname << std::endl;
        return -1;
    }

    double scale = (argc>5) ? atoi(argv[5]) : 5;
    sif.scale(height/scale);

	// reset rectangles to random positions
	for(unsigned i=0;i<sif.nodes.size(); ++i) {
		double x=getRand(width), y=getRand(height);
		sif.nodes[i]->moveCentre(x,y);
	}

	// apply scaled majorization layout
	ConstrainedMajorizationLayout alg(sif.nodes,sif.edges,NULL,1,sif.idealLengths);
	alg.setScaling(true);
	alg.run();
	
	std::ostream& out = std::cout;
	printCyJsPrefix(out);
    sif.print(out);
    out << "}" << std::endl;

    return 0;
}


/*
	CompoundConstraints ccs;
	AlignmentConstraint ac(vpsc::XDIM);
	ccs.push_back(&ac);
	ac.addShape(0,0);
	ac.addShape(3,0);
*/
	//alg.setConstraints(&ccs);
	// apply steepest descent layout
	//ConstrainedFDLayout alg2(rs,es,width/2, false);
	//alg2.setConstraints(ccs);
	//alg2.run();
    //alg.outputInstanceToSVG("testIdeal");
    //OutputFile output(sif.nodes,sif.edges,NULL,"test.svg");
    //output.generate();
